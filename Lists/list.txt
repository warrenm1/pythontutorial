**Lists**

Python uses lists to group together other values. It  can be written as a list of comma-separated values (items) between square brackets. Lists might contain items of different types, but usually the items all have the same type.

    squares = [1, 4, 9, 16, 25]
    print (squares)
    >> [1, 4, 9, 16, 25]
    need = ['bananas', 'milk', 'ice cream', 'broccoli', 'spinach']
    print ("what I need is ", need)
    >> what I need is ['bananas', 'milk', 'ice cream', 'broccoli', 'spinach']

Lists can be looked at one element at a time.  The first element is element 0. 

    print (squares[0])  # indexing returns the item
    >> 1
    print (squares[2])  # indexing returns the item
    >> 9

     print (squares[-1]) #last element of the list 
    >> 25

We can also pull out a section at a time (termed a slice).  A slice is specified by giving the first index to be included followed by a : followed by the last index.  Note the  item at the last index is not included.

    print (squares[0:2])  # slicing returns a new list
    >>[1,4]
    print (squares[-3:]) # slicing returns a new list 
    [9, 16, 25]

All slice operations return a new list containing the requested elements. This means that the following slice returns a new (shallow) copy of the list:

    print (squares[:])  # if the beginning is omitted,  0 is assumed.  
                   #If the ending is omitted, the end of the list is assumed.
    >> [1, 4, 9, 16, 25]

Lists also support operations like concatenation:

    print ( squares + [36, 49, 64, 81, 100])
    >> [1, 4, 9, 16, 25, 36, 49, 64, 81, 100]

Unlike strings, which can't be changed (without creating a copy), lists are a mutable (Links to an external site.)Links to an external site. type, i.e. it is possible to change their content:

    cubes = [1, 8, 27, 65, 125]  # something's wrong here
    cubes[3] = 64  # replace the wrong value
    print(cubes)
    >> [1, 8, 27, 64, 125]
    print ("cubes are", cubes)
    >> cubes are [1, 8, 27, 64, 125]  # can print two things if comma separated

 

You can also add new items at the end of the list, by using the append() method (we will see more about methods later):

    cubes.append(216)  # add the cube of 6
    cubes.append(7 ** 3)  # and the cube of 7
    print( cubes)
    >>[1, 8, 27, 64, 125, 216, 343]

Assignment to slices is also possible, and this can even change the size of the list or clear it entirely:

    letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g']
    print (letters)
    >> ['a', 'b', 'c', 'd', 'e', 'f', 'g']
    # replace some values
    letters[2:5] = ['C', 'D', 'E']
    print(letters)
    >>['a', 'b', 'C', 'D', 'E', 'f', 'g']
    # now remove them
    letters[2:5] = []
    print(letters)
    >>['a', 'b', 'f', 'g']
    # clear the list by replacing all the elements with an empty list
    letters[:] = []
    print(letters)
    >>[]

The built-in function len() (Links to an external site.)Links to an external site. also applies to lists:

    letters = ['a', 'b', 'c', 'd']
    print( len(letters))
    >> 4
 

*Challenge*

Take a look at the following code:

Divide the list into two separate lists: boys and girls.

Sort the individual lists.

Lists of Lists

It is possible to nest lists (create lists containing other lists), for example:

    a = ['a', 'b', 'c']
    n = [1, 2, 3]
    x = [a, n]
    print(x)
    >> [['a', 'b', 'c'], [1, 2, 3]]
    print( x[0])
    >> ['a', 'b', 'c']
    print( x[0][1])
    >> b 

What do you expect to happen if you do

    a[2] = 'dog'

Has list x changed?  How can you explain  your results?
We say the copy is shallow.  A shallow copy is a copy of the structure, not of the individual elements.  A deep copy (on the other hand) means you have duplicated values.
So for example, if I have an envelop with $100 in it and I make a shallow copy (one for you and one for me), either one of us can spent it, but there is only $100 total.
Now, if I have an envelop with $100 in it and I make a deep copy (one for you and one for me), each one of us can spent our $100. There is $200 total.